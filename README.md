# compound

Simple repo to test Fyne Golang framework to do GUI apps, in this case for Compount Interest calculation

To install Fyne:
`go install fyne.io/fyne/v2/cmd/fyne`

it can compile to any OS:

```
# Current
fyne package
# Android (need of SDK and NDK)
fyne package -os android -appID com.master.password
```