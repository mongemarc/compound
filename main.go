package main

// go build -ldflags -H=windowsgui -o encode.exe
// go install fyne.io/fyne/v2/cmd/fyne && fyne package (-os android -appID com.master.password)

import (
	"image/color"
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

func compoundInterestWithAddition(principal, rate, time float64, compPerYear int, annualAddition float64) float64 {
	// Convert annual rate to decimal
	r := rate / 100.0
	factor := (1 + r/float64(compPerYear))

	// Calculate compound interest with annual addition
	var amount float64
	amount = principal
	for i := 0; i < int(time)*compPerYear; i++ {
		amount *= factor
		amount += annualAddition
	}

	return amount
}

func main() {
	a := app.New()
	w := a.NewWindow("Compound Interest")

	principal := widget.NewEntry()
	rate := widget.NewEntry()
	time := widget.NewEntry()
	compPerYear := widget.NewEntry()
	addition := widget.NewEntry()

	output := canvas.NewText("", color.White)
	button := widget.NewButton("Calculate", func() {
		p, _ := strconv.ParseFloat(principal.Text, 64)
		r, _ := strconv.ParseFloat(rate.Text, 64)
		t, _ := strconv.ParseFloat(time.Text, 64)
		c, _ := strconv.Atoi(compPerYear.Text)
		a, _ := strconv.ParseFloat(addition.Text, 64)
		output.Text = strconv.FormatFloat(compoundInterestWithAddition(p, r, t, c, a), 'f', -1, 64)
		output.Refresh()
	})
	copyButton := widget.NewButtonWithIcon("Copy", theme.ContentCopyIcon(), func() {
		if output.Text != "" {
			w.Clipboard().SetContent(output.Text)
		}
	})

	// Layout
	w.Resize(fyne.NewSize(400, 600))
	w.SetContent(container.NewCenter(
		container.NewVBox(
			widget.NewLabel("Initial investment amount:"),
			principal,
			widget.NewLabel("Annual interest rate (in percentage):"),
			rate,
			widget.NewLabel("Time the money is invested or borrowed for (in years):"),
			time,
			widget.NewLabel("Number of times interest is compounded per year:"),
			compPerYear,
			widget.NewLabel("Addition of cash (compPerYear times):"),
			addition,
			widget.NewLabel("Output:"),
			output,
			container.NewHBox(
				button,
				copyButton,
			),
		),
	))
	w.ShowAndRun()
}
